#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os


# In[2]:


os.system('python3 -m pip install tensorflow')


# In[3]:


import tensorflow as tf


# In[4]:


import pandas as pd


# In[5]:


import numpy as np


# In[6]:


import matplotlib.pyplot as plt


# In[7]:


df = pd.read_csv('BTCUSDT-w08-2019-latest.csv')


# In[8]:


from datetime import datetime


# In[9]:


start_time = [datetime.utcfromtimestamp(float(df.columns[0].split(' ')[1].split('=')[1])).strftime('%Y-%m-%d %H:%M:%S')]


# In[10]:


start_time


# In[11]:


sells = np.array([float(x[0].split(' ')[1]) for x in df.values[1:]])


# In[12]:


sells[0]


# In[13]:


sells[3]


# In[14]:


buys = np.array([float(x[0].split(' ')[2]) for x in df.values[1:]])


# In[15]:


buys[0]


# In[16]:


buys[5]


# In[17]:


avgs = (sells + buys) / 2


# In[18]:


time_stamps = [datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x in [float(y[0].split(' ')[0]) for y in df.values[1:]]]


# In[19]:


time_stamps[0]


# In[20]:


time_stamps[1]


# In[21]:


data = pd.DataFrame(index=time_stamps, data={'Avgs': avgs, 'Sells': sells,
                                                'Buys': buys,
                                                'Start': start_time})


# In[22]:


data.head()


# In[23]:


data.columns


# In[24]:


data.index


# In[25]:


X = [data[(x * 60):((x + 1) * 60)] for x
             in range(len(data) // 60)]


# In[26]:


X[0]


# In[27]:


Y = data[::60]


# In[28]:


Y


# In[29]:


X_train = X[:(4 * len(X) // 5)]
Y_train = Y[:(4 * len(Y) // 5)]
X_val = X[(4 * len(X) // 5):]
Y_val = Y[(4 * len(Y) // 5):]


# In[30]:


X_data = [X_train, X_val]
Y_data = [Y_train, Y_val]


# In[31]:


import matplotlib as mpl


# In[32]:


mpl.rcParams['figure.figsize'] = (8, 6)
mpl.rcParams['axes.grid'] = False


# In[33]:


X[0]['Avgs']


# In[34]:


uni_data = [X[i]['Avgs'] for i in range(len(X))]


# In[35]:


len(uni_data)


# In[36]:


len(X)


# In[37]:


uni_data[0].plot


# In[38]:


get_ipython().run_line_magic('matplotlib', 'inline')


# In[39]:


uni_data[0].plot


# In[40]:


plt.plot(uni_data[0])


# In[41]:


plt.xticks(rotation=45)


# In[42]:


plt.plot(uni_data[0])


# In[43]:


plt.plot(uni_data[0].values)


# In[44]:


uni_data[0].index


# In[45]:


plt.plot(uni_data[0].index, uni_data[0].values)


# In[46]:


fig, ax = plt.subplots()


# In[47]:


ax.plot(uni_data[0].index, uni_data[0].values)


# In[48]:


for label in ax.get_xticklabels():
    label.set_rotation(45)


# In[49]:


plt.show()


# In[50]:


plt.plot(uni_data[0].index, uni_data[0].values)


# In[51]:


plt.setp(ax.get_xticklabels(), rotation=45)


# In[52]:


plt.xticks(rotation=45)


# In[53]:


fig.autofmt_xdate()


# In[54]:


labels = uni_data[0].index


# In[55]:


fig, ax = plt.subplots()


# In[56]:


ax.set_xticklabels(labels, rotation=45, ha='right')


# In[57]:


fig, ax = plt.subplots()
ax.plot(uni_data[0].index, uni_data[0].values)
plt.xticks(rotation=45)


# In[58]:


fig, ax = plt.subplots()
ax.plot(uni_data[0].index, uni_data[0].values)
plt.xticks(rotation=90)


# In[59]:


fig, ax = plt.subplots(figsize=(40, 40))
ax.plot(uni_data[0].index, uni_data[0].values)
plt.xticks(rotation=90)


# In[60]:


plt.plot(uni_data[0].values)


# In[61]:


uni_data_mean = [x.mean() for x in uni_data]


# In[62]:


uni_data_mean[0]


# In[63]:


uni_data_mean[1]


# In[64]:


uni_data_std = [x.std() for x in uni_data]


# In[65]:


plt.plot(uni_data_std)


# In[66]:


plt.plot(uni_data_mean)


# In[67]:


BATCH_SIZE = 256


# In[68]:


len(X_train)


# In[69]:


BUFFER_SIZE = 10000


# In[70]:


len(Y_train)


# In[71]:


len(X_train)


# In[72]:


X_train[:10]


# In[73]:


X_train_avgs = [x['Avgs'] for x in X_train]


# In[74]:


X_train_avgs[:10]


# In[75]:


len(X_train_avgs)


# In[76]:


Y_train[:10]


# In[77]:


Y_train['Avgs']


# In[78]:


Y_train_avgs = Y_train['Avgs']


# In[79]:


train_uni = tf.data.Dataset.from_tensor_slices((X_train_avgs, Y_train_avgs))


# In[83]:


dir(tf)


# In[84]:


train_uni = train_uni.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()


# In[87]:


X_val


# In[88]:


X_val_avgs = X_val['Avgs']


# In[89]:


X_val_avgs = [x['Avgs'] for x in X_val]


# In[90]:


X_val_avgs[0]


# In[91]:


Y_val


# In[92]:


Y_val_avgs = Y_val['Avgs']


# In[93]:


len(X_val_avgs)


# In[94]:


val_uni = tf.data.Dataset.from_tensor_slices((X_val_avgs, Y_val_avgs))


# In[95]:


val_uni = val_uni.batch(BATCH_SIZE).repeat()


# In[103]:


np.reshape(np.array(X_train_avgs[0]), (60, ))


# In[104]:


np.reshape(np.array(X_train_avgs[0]), (60, 1))


# In[105]:


X_train_avgs2 = [np.reshape(np.array(x), (60, 1)) for x in X_train_avgs]


# In[106]:


X_val_avgs2 = [np.reshape(np.array(x), (60, 1)) for x in X_val_avgs]


# In[107]:


train_uni2 = tf.data.Dataset.from_tensor_slices((X_train_avgs2, Y_train_avgs))


# In[108]:


train_uni2 = train_uni2.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()


# In[109]:


val_uni2 = tf.data.Dataset.from_tensor_slices((X_val_avgs2, Y_val_avgs))


# In[110]:


val_uni2 = val_uni2.batch(BATCH_SIZE).repeat()


# In[112]:


simple_lstm = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(8, input_shape=np.array(X_train_avgs2).shape[-2:]),
    tf.keras.layers.Dense(1)
])

simple_lstm.compile(optimizer='adam', loss='mae')


# In[114]:


for x, y in val_uni2.take(1):
    print(simple_lstm.predict(x).shape)


# In[115]:


EVALUATION_INTERVAL = 200
EPOCHS = 10

simple_lstm.fit(train_uni2, epochs=EPOCHS,
                steps_per_epoch=EVALUATION_INTERVAL,
               validation_data=val_uni2, validation_steps=50)


# In[120]:


for x, y in val_uni2.take(3):
    tf.print(simple_lstm.predict(x)[0])


# In[121]:


for x, y in val_uni2.take(3):
    tf.print(simple_lstm.predict(x)[0])
    tf.print(x[0].numpy())
    tf.print(y[0].numpy())


# In[122]:


for x, y in val_uni2.take(3):
    tf.print(simple_lstm.predict(x)[0])
    tf.print(y[0].numpy())


# In[123]:


for x, y in val_uni2.take(3):
    tf.print(simple_lstm.predict(x))
    tf.print(y[0].numpy())


# In[125]:


X_train[0]['Avgs']


# In[126]:


def uni_data(dataset, start_index, end_index, history_size, target_size):
    data = []
    labels = []
    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size
        
    for i in range(start_index, end_index):
        indices = range(i-history_size, i)
        # Reshape data from (histroy_size, ) to (history_size, 1)
        data.append(np.reshape(dataset[indices], (history_size, 1)))
        labels.append(dataset[i+target_size])
    return np.array(data), np.array(labels)


# In[127]:


len(data['Avgs'])


# In[128]:


end = len(data['Avgs']) * 4 // 5


# In[129]:


end


# In[131]:


data['Avgs'].shape


# In[132]:


def uni_data(dataset, start_index, end_index, history_size, target_size):
    data = []
    labels = []
    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size
        
    for i in range(start_index, end_index):
        indices = range(i-history_size, i)
        # Reshape data from (histroy_size, ) to (history_size, 1)
        data.append(np.reshape(np.array(dataset[indices]), (history_size, 1)))
        labels.append(dataset[i+target_size])
    return np.array(data), np.array(labels)


# In[135]:


np.array(data['Avgs'])[60]


# In[137]:


def uni_data(dataset, start_index, end_index, history_size, target_size):
    data = []
    labels = []
    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size
        
    for i in range(start_index, end_index):
        indices = range(i-history_size, i)
        # Reshape data from (histroy_size, ) to (history_size, 1)
        data.append(np.reshape(np.array(dataset[indices]), (history_size, 1)))
        labels.append(np.array(dataset[i+target_size]))
    return np.array(data), np.array(labels)


# In[139]:


x_train_uni, y_train_uni = uni_data(np.array(data['Avgs']), 0, 2552496, 60, 0)


# In[140]:


x_train_uni[0]


# In[142]:


x_train_uni[0][0]


# In[145]:


data['Avgs']


# In[147]:


np.array(data['Avgs'])[:60]


# In[148]:


x_train_uni[0] == np.array(data['Avgs'])[:60]


# In[149]:


x_train_uni[0][1]


# In[150]:


np.array(data['Avgs'])[1]


# In[151]:


x_train_uni[0].shape


# In[152]:


np.array(data['Avgs'])[:60].shape


# In[153]:


ttt = np.reshape(np.array(data['Avgs'])[:60], (60, 1))


# In[154]:


x_train_uni[0] == ttt


# In[155]:


x_val_uni, y_val_uni = uni_data(np.array(data['Avgs']), 2552496, None, 60, 0)


# In[156]:


x_train_uni[0]


# In[157]:


y_train_uni[0]


# In[158]:


test_train_uni = tf.data.Dataset.from_tensor_slices((x_train_uni, y_train_uni))


# In[159]:


test_train_uni = test_train_uni.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()


# In[160]:


test_val_uni = tf.data.Dataset.from_tensor_slices((x_val_uni, y_val_uni))


# In[161]:


test_val_uni = test_val_uni.batch(BATCH_SIZE).repeat()


# In[162]:


test_lstm = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(8, input_shape=x_train_uni.shape[-2:]),
    tf.keras.layers.Dense(1)
])

test_lstm.compile(optimizer='adam', loss='mae')


# In[163]:


for x, y in test_val_uni.take(1):
    print(test_lstm.predict(x).shape)


# In[164]:


test_lstm.fit(test_train_uni, epochs=EPOCHS,
             steps_per_epoch=EVALUATION_INTERVAL,
             validation_data=test_val_uni, validation_steps=50)


# In[165]:


for x, y in test_val_uni.take(3):
    tf.print(y[0].numpy())
    tf.print(test_lstm.predict(x)[0])


# In[166]:


x_train_uni.shape


# In[167]:


y_train_uni.shape


# In[175]:


no_repeat_data_lstm = test_lstm.fit(
    x_train_uni, y_train_uni,
    epochs=30,
    batch_size=256
)


# In[177]:


x_train_uni.shape


# In[178]:


y_train_uni.shape


# In[179]:


y_train_uni


# In[180]:


test_lstm.fit(test_train_uni, epochs=100,
             steps_per_epoch=EVALUATION_INTERVAL,
             validation_data=test_val_uni, validation_steps=50)


# In[182]:


test_lstm = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(8, input_shape=x_train_uni.shape[-2:]),
    tf.keras.layers.Dense(1)
])

test_lstm.compile(optimizer='SGD', loss='binary_crossentropy')


# In[183]:


test_lstm.fit(test_train_uni, epochs=100,
             steps_per_epoch=EVALUATION_INTERVAL,
             validation_data=test_val_uni, validation_steps=50)


# In[184]:


for x, y in test_val_uni.take(3):
    tf.print(y[0].numpy())
    tf.print(test_lstm.predict(x)[0])


# In[ ]:




