# pylint: disable=C0103
# import tensorflow as tf
# import matploblib as mpl
# import matplotlib.pyplot as plt
# import numpy as np
# import os
# from datetime import datetime
from utility import get_data
# import pandas as pd
# import csv


class Predictor:
    """Price predictor model"""
    def __init__(self, settings):
        self.settings = settings
        self.data = get_data()
        self.X = []
        self.Y = []

    def train_validate_split(self):
        window_size = self.settings['window_size']
        X = [self.data[(x * window_size):((x + 1) * window_size)] for x
             in range(len(self.data) // window_size)]
        Y = self.data[::window_size]
        X_train = X[:(4 * len(X) // 5)]
        Y_train = Y[:(4 * len(Y) // 5)]
        X_val = X[(4 * len(X) // 5):]
        Y_val = Y[(4 * len(Y) // 5):]
        self.X = [X_train, X_val]
        self.Y = [Y_train, Y_val]

    def draw(self):
        pass


if __name__ == '__main__':
    SETTINGS = {'window_size': 60}
    Predictor(SETTINGS)
