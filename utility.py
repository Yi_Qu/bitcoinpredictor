"""Utility functions"""
from datetime import datetime
import numpy as np
import pandas as pd


def get_data(file_name='./BTCUSDT-w08-2019-latest.csv'):
    data_frame = pd.read_csv(file_name)
    start_time = unix_timestamp_converter(
        [float(data_frame.columns[0].split(' ')[1].split('=')[1])])
    sells = np.array([float(x[0].split(' ')[1]) for x in
                      data_frame.values[1:]])
    buys = np.array([float(x[0].split(' ')[2]) for x in data_frame.values[1:]])
    avgs = (sells + buys) / 2
    time_stamps = unix_timestamp_converter(
        [float(x[0].split(' ')[0]) for x in data_frame.values[1:]])
    ret = pd.DataFrame(index=time_stamps, data={'Avgs': avgs, 'Sells': sells,
                                                'Buys': buys,
                                                'Start': start_time})
    # ret.to_csv('out.csv', index=True)
    return ret


def unix_timestamp_converter(unix_timestamp):
    return [datetime.utcfromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S') for x
            in unix_timestamp]
